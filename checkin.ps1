param(
    # Parameter help description
    [string]
    $CommitMessage,

    [switch]
    $AutoConfirm
)
git branch

Write-Host "Running git pull"
git pull

Write-Host "Starting  git add dry run"
git add -n .
$Confirm = ""

if($AutoConfirm)
{
    $Confirm = "yes"
}
else 
{
    $Confirm = Read-Host -Prompt "Do you want to commit the above changes?(Yes/No)?"
}

if($Confirm.ToUpper() -eq "YES") 
{
    Write-Host "Starting  git add"
    git add .

    Write-Host "Starting  git commit"
    if($CommitMessage.Trim().Length -gt 0)
    {
        git commit -m $CommitMessage
    }
    else 
    {
        git commit
    }

    Write-Host "Starting  git push"
    git push
    Write-Host "Success pushed to the current branch"
}
else {
    Write-Host "Changes are not pushed. "
}