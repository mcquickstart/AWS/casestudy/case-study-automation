module "random" {
  source = "../../../modules/random"
}

output "rand_value" {
  value = module.random.random_value
}