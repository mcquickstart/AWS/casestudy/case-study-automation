terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "3.5.1"
    }
  }

  backend "s3" {
    bucket  = "rsrawspractice-tf-state-files"
    encrypt = true
    key     = "AIA/dev/random/terraform.state"
    region  = "ap-south-1"
  }
}