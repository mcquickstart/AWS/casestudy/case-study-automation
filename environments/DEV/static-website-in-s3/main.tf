module "sample" {
  source         = "../../../modules/static-website-in-s3"
  appCode        = "SWSD"
  applictionName = "static-web-site-demo"
  environment    = "sandbox"
  bucket_versioning = {
    "versioning_enabled" = true
    "mfa_delete"         = false
  }
  block_public_access = {
    "block_public_acls"       = true
    "block_public_policy"     = true
    "ignore_public_acls"      = true
    "restrict_public_buckets" = true
  }
}