terraform {
  required_version = ">= 0.13.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.0"
    }
  }

  backend "s3" {
    bucket  = "rsrawspractice-tf-state-files"
    encrypt = true
    key     = "AIA/dev/static-website-in-s3/terraform.state"
    region  = "ap-south-1"
  }
}