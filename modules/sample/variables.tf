locals {
    common_tags = {
        "Environment" = var.environment, 
        "Application" = var.application,
        "Automation" = "Yes"
    }

    free_tags = {
        "Cost" = "Free"
    }
    
    keep_alive_tags = {
        "KeepAlive" = "Yes"
    }
}

variable "environment" {
    type = string
    description = "Specify the environment - dev, qa or production"
    validation {
        condition = contains(["DEV", "QA", "PROD"], upper(var.environment))
        error_message = "Environment should one of dev, qa and prod"
    }
    default = "DEV"
}

variable "application" {
    type = string
    description = "Specify the name of the application"
    default = "ACSA"
}
