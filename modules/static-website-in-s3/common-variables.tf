locals {
    common_tags = {
        "Environment" = var.environment, 
        "AppCode" = var.appCode,
        "Automation" = "Yes",
        "Application" = var.applictionName
    }

    free_tags = {
        "Cost" = "Free"
    }
    
    keep_alive_tags = {
        "KeepAlive" = "Yes"
    }

    sandbox_tag = {
        "Sandbox" = var.Sandbox
    }

    bucket_name = "${length(var.bucket_name)>0?var.bucket_name:format("%s-%s-%s", lower(var.appCode), lower(var.applictionName), lower(var.environment))}"
}

variable "environment" {
    type = string
    description = "Specify the environment - dev, qa or production"
    validation {
        condition = contains(["sandbox", "dev", "qa", "prod"], var.environment)
        error_message = "Environment should one of dev, qa and prod"
    }
    default = "sandbox"
}

variable "appCode" {
    type = string
    description = "Specify the code of the application"
    default = "ACSA"
}

variable "applictionName" {
    type = string
    description = "Specify the name of the application"
}

variable "Sandbox" {
    type = string
    description = "Specify if the resource is to be treated as a sandbox."
    default = "No"
}
