###############################################################################################
# Description: This module will create a S3 bucket with versioning and public access control. 
# Change log:                                      
# 2023-11-22: V1.0: Initial version
# Documentation: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket
###############################################################################################


resource "aws_s3_bucket" "swsd" {
    bucket = local.bucket_name
    tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        local.sandbox_tag
    )
}

resource "aws_s3_bucket_ownership_controls" "swsd_ownership" {
  bucket = aws_s3_bucket.swsd.id
  rule {
    object_ownership = var.bucket_ownership
  }
  depends_on = [ aws_s3_bucket.swsd ]
}

resource "aws_s3_bucket_versioning" "swsd_versioning" {
  bucket = aws_s3_bucket.swsd.id
  versioning_configuration {
    status = try(var.bucket_versioning["versioning_enabled"] ? "Enabled" : "Suspended", "Enabled")
    mfa_delete = try(var.bucket_versioning["mfa_delete"] ? "Enabled" : "Disabled", "Disabled")
  }
  depends_on = [ aws_s3_bucket.swsd ]
}

resource "aws_s3_bucket_public_access_block" "swsd_block_public_access" {
  bucket = aws_s3_bucket.swsd.id
  block_public_acls = try(var.block_public_access["block_public_acls"] ? true : false, true)
  block_public_policy = try(var.block_public_access["block_public_policy"] ? true : false, true)
  ignore_public_acls = try(var.block_public_access["ignore_public_acls"] ? true : false, true)
  restrict_public_buckets = try(var.block_public_access["restrict_public_buckets"] ? true : false, true) 
}