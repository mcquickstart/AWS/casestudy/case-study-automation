resource "aws_cloudfront_distribution" "swsd_cloudfront" {
  origin {
    domain_name = aws_s3_bucket.swsd.bucket_regional_domain_name
    origin_access_control_id = aws_cloudfront_origin_access_control.swsd_cloudfront_oac.id
    origin_id = aws_s3_bucket.swsd.bucket_regional_domain_name
  }
  enabled = true
  is_ipv6_enabled = true
  comment = "SWSD: Static Website demo. Cloufront."
  default_root_object = "index.html"
  price_class = "PriceClass_All"
  aliases = ["static-dev.rsroy.com"]
  default_cache_behavior {
    allowed_methods =  ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.swsd.bucket_regional_domain_name
    compress = true
    viewer_protocol_policy = "https-only"
    smooth_streaming = false
    cache_policy_id = data.aws_cloudfront_cache_policy.swsd_cache_policy.id
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn = aws_acm_certificate.swsd_certificate.arn
    minimum_protocol_version = "TLSv1.2_2021"
    ssl_support_method = "sni-only"
  }

  tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        local.sandbox_tag
    )
}

resource "aws_cloudfront_origin_access_control" "swsd_cloudfront_oac" {
  name = aws_s3_bucket.swsd.bucket_regional_domain_name
  description = "Control setting for static site demo."
  origin_access_control_origin_type = "s3"
  signing_behavior = "always"
  signing_protocol = "sigv4"
}

resource "aws_s3_bucket_policy" "swsd_bucket_policy" {
  bucket = aws_s3_bucket.swsd.id
  policy = data.aws_iam_policy_document.swsd_bucket_policy_cloudfront.json
}