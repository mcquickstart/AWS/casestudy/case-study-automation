data "aws_iam_policy_document" "swsd_bucket_policy_cloudfront" {
  statement {
    sid = "AllowCloudFrontServicePrincipal"
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [ "cloudfront.amazonaws.com" ]
    }
    actions = [ 
         "s3:GetObject"
     ]
     resources = [ 
        aws_s3_bucket.swsd.arn,
        "${aws_s3_bucket.swsd.arn}/*"
      ]
      condition {
        test = "StringEquals"
        variable = "AWS:SourceArn"
        values = [resource.aws_cloudfront_distribution.swsd_cloudfront.arn]
      } 
  }
}

data "aws_route53_zone" "swsd_hosted_zone" {
  name         = "rsroy.com"
  private_zone = false
}

data "aws_cloudfront_cache_policy" "swsd_cache_policy" {
  name = "Managed-CachingOptimized"
}