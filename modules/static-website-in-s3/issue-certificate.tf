resource "aws_acm_certificate" "swsd_certificate" {
  domain_name = "static-dev.rsroy.com"
  subject_alternative_names = ["static-dev.rsroy.com", "*.rsroy.com"]
  validation_method = "DNS"
  tags = merge(
        local.common_tags,
        local.free_tags,
        local.keep_alive_tags,
        local.sandbox_tag
    )
  lifecycle {
    create_before_destroy = true 
  }
}

resource "aws_route53_record" "swsd_certificate_r53_entry" {
  for_each = {
    for dvo in aws_acm_certificate.swsd_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.swsd_hosted_zone.zone_id
}

resource "aws_acm_certificate_validation" "swsd_certificate_validation" {
  certificate_arn         = aws_acm_certificate.swsd_certificate.arn
  validation_record_fqdns = [for record in aws_route53_record.swsd_certificate_r53_entry : record.fqdn]
}