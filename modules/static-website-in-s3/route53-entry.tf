resource "aws_route53_record" "swsd_r53_entry" {
  zone_id = data.aws_route53_zone.swsd_hosted_zone.zone_id
  name = "static-dev.rsroy.com"
  type = "A"
  alias {
    evaluate_target_health = false
    zone_id = aws_cloudfront_distribution.swsd_cloudfront.hosted_zone_id
    name = aws_cloudfront_distribution.swsd_cloudfront.domain_name
  }
}