variable "bucket_name" {
  type = string
  description = "Specify the name of the bucket."
  default = ""
  validation {
    condition = (var.bucket_name == lower(var.bucket_name))
    error_message = "Bucket name should have lowercase values only."
  } 
}

variable "bucket_ownership" {
  type = string
  description = "Specify the bucket ownership"
  default = "BucketOwnerEnforced"
  validation {
    condition = contains(["BucketOwnerPreferred", "ObjectWriter", "BucketOwnerEnforced"], var.bucket_ownership)
    error_message = "Valid values are BucketOwnerPreferred, ObjectWriter or BucketOwnerEnforced."
  }
}

variable "bucket_versioning" {
  type = map(bool)
  description = "Specify the bucket version configuration."
  default = {
    "versioning_enabled" = true,
    "mfa_delete" = false
  }

  validation {
    condition = (contains(keys(var.bucket_versioning), "versioning_enabled") && contains(keys(var.bucket_versioning), "mfa_delete"))
    error_message = "Missing parameter from bucket versioning. Please specify both versioning_enabled and mfa_delete attributes."
  }
}

variable "block_public_access" {
  type = map(bool)
  description = "Specify block public access parameters. By default block all public access."
  default = {
    "block_public_acls" = true
    "block_public_policy" = true
    "ignore_public_acls" = true
    "restrict_public_buckets" = true
  }

  validation {
    condition = (contains(keys(var.block_public_access), "block_public_acls") || contains(keys(var.block_public_access), "block_public_policy") || contains(keys(var.block_public_access), "ignore_public_acls") || contains(keys(var.block_public_access), "restrict_public_buckets"))
    error_message = "block_public_access. Please specify at least one of the following policies - block_public_acls, block_public_policy, ignore_public_acls and restrict_public_buckets."
  }
  
}